import 'dart:async';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'c_data.dart';
import 'history_list.dart';
import 'keyboard.dart';

void main() => runApp(MyApp());

var data = '0';

class MyApp extends StatelessWidget {
  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    HistoryList.getHistoryList();

    return MaterialApp(
      title: 'Flutter Demo',
      theme: ThemeData(
        primarySwatch: Colors.lightBlue,
      ),
      home: MyHomePage(title: "计算器"),
    );
  }
}

class MyHomePage extends StatefulWidget {
  MyHomePage({Key key, this.title}) : super(key: key);

  final String title;

  @override
  _MyHomePageState createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {
  List _historyList = Cdata.historyList;
  FocusNode _contentFocusNode = FocusNode();

  @override
  void initState() {
    super.initState();
    _historyList = Cdata.historyListV.value;
  }

  Widget history(BuildContext ctx, int index) {
    return GestureDetector(
        // 点击当前行触发
        onTap: () {
          // 设置当前计算数据为选中历史表达式
          String tempStr=Cdata.historyListV.value[index]
              .toString()
              .split(",")[1]
              .split("=")[0];
          Cdata.setData(tempStr);
          Cdata.baseOffset=tempStr.length;
          Cdata.setCursor();

        },
        child: Dismissible(
          key: Key(UniqueKey().toString()),
          onDismissed: (_) {
            Cdata.historyList.removeAt(index);
            HistoryList.syncHistoryList();
            HistoryList.setHistoryList("");
          },
          background: new Container(
              color: Colors.deepOrangeAccent,
              child: Row(
                mainAxisAlignment: MainAxisAlignment.end,
                children: <Widget>[
                  Padding(
                    //左边添加8像素补白
                    padding: const EdgeInsets.only(right: 20.0),
                    child: Text(
                      "删除",
                      style: TextStyle(color: Colors.white, fontSize: 24),
                    ),
                  ),
                ],
              )),
          child: Container(
            key: Key(UniqueKey().toString()),
            child: Row(
              children: <Widget>[
                Expanded(
                  flex: 1,
                  child: ListTile(
                    leading: Icon(Icons.history),
                    title: Text(
                      Cdata.historyListV.value[index].toString().split(",")[1],
                      style: TextStyle(fontSize: 20),
                    ),
                    subtitle: Text(Cdata.historyListV.value[index]
                        .toString()
                        .split(",")[0]),
                  ),
                ),
              ],
            ),
            // 下边框
            decoration: BoxDecoration(
                border: Border(
                    bottom: BorderSide(width: 1, color: Color(0xffe5e5e5)))),
          ),
        ));
  }

  double buttonHeight = 75.0;
  @override
  Widget build(BuildContext context) {
    Timer(
        Duration(milliseconds: 50),
        () => Cdata.listViewController
            .jumpTo(Cdata.listViewController.position.maxScrollExtent + 100));

    return Scaffold(
//      appBar: AppBar(
//        // Here we take the value from the MyHomePage object that was created by
//        // the App.build method, and use it to set our appbar title.
//        title: Text(widget.title),
//      ),
      body: Center(
        // Center is a layout widget. It takes a single child and positions it
        // in the middle of the parent.
        child: Column(
          mainAxisAlignment: MainAxisAlignment.end,
          children: <Widget>[
            Expanded(
              flex: 1,
              child: ValueListenableBuilder(
                  valueListenable: Cdata.historyListV,
                  builder: (BuildContext context, List list, Widget child) {
                    return new ListView.builder(
                        controller: Cdata.listViewController,
                        itemCount: Cdata.historyListV.value == null
                            ? 0
                            : Cdata.historyListV.value.length,
                        itemBuilder: history);
                  }),
            ),
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: <Widget>[
                new Expanded(
                  child: TextField(
                    readOnly: false,
                    enableInteractiveSelection: true,
                    decoration: InputDecoration(
                      hintText: "0",
                      contentPadding: EdgeInsets.only(right: 7,left: 3,bottom: 12,top: 12),
                      border: InputBorder.none
                    ),
                    controller: Cdata.editingController,
                    textAlign: TextAlign.right,
                    style: TextStyle(
                      fontSize: 36,
                    ),
                    maxLines: 6,
                    minLines: 1,
                    onTap: (){
                      Cdata.baseOffset=Cdata.editingController.selection.baseOffset;
                    },
                  ),
                )
              ],
            ),
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceAround,
              children: <Widget>[
                Expanded(
                  child: Container(
                    height: buttonHeight,
                    child: CancelButton(),
                  ),
                ),
                Expanded(
                  flex: 2,
                  child: Container(
                    height: buttonHeight,
                    child: CalculatorButton(),
                  ),
                ),
                Expanded(
                  child: Container(
                    height: buttonHeight,
                    child: DeleteButton(),
                  ),
                ),
              ],
            ),
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceAround,
              children: <Widget>[
                Expanded(
                  child: Container(
                    height: buttonHeight,
                    child: Numbutton("7", color: Colors.white),
                  ),
                ),
                Expanded(
                  child: Container(
                    height: buttonHeight,
                    child: Numbutton(
                      "8",
                      color: Colors.white,
                    ),
                  ),
                ),
                Expanded(
                  child: Container(
                    height: buttonHeight,
                    child: Numbutton(
                      "9",
                      color: Colors.white,
                    ),
                  ),
                ),
                Expanded(
                  child: Container(
                    height: buttonHeight,
                    child: Numbutton("+"),
                  ),
                ),
              ],
            ),
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceAround,
              children: <Widget>[
                Expanded(
                  child: Container(
                    height: buttonHeight,
                    child: Numbutton("4", color: Colors.white),
                  ),
                ),
                Expanded(
                  child: Container(
                    height: buttonHeight,
                    child: Numbutton("5", color: Colors.white),
                  ),
                ),
                Expanded(
                  child: Container(
                    height: buttonHeight,
                    child: Numbutton("6", color: Colors.white),
                  ),
                ),
                Expanded(
                  child: Container(
                    height: buttonHeight,
                    child: Numbutton("-"),
                  ),
                ),
              ],
            ),
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceAround,
              children: <Widget>[
                Expanded(
                  child: Container(
                    height: buttonHeight,
                    child: Numbutton("1", color: Colors.white),
                  ),
                ),
                Expanded(
                  child: Container(
                    height: buttonHeight,
                    child: Numbutton("2", color: Colors.white),
                  ),
                ),
                Expanded(
                  child: Container(
                    height: buttonHeight,
                    child: Numbutton("3", color: Colors.white),
                  ),
                ),
                Expanded(
                  child: Container(
                    height: buttonHeight,
                    child: Numbutton("*"),
                  ),
                ),
              ],
            ),
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceAround,
              children: <Widget>[
                Expanded(
                  child: Container(
                    height: buttonHeight,
                    child: RotateButton(),
                  ),
                ),
                Expanded(
                  child: Container(
                    height: buttonHeight,
                    child: Numbutton("0", color: Colors.white),
                  ),
                ),
                Expanded(
                  child: Container(
                    height: buttonHeight,
                    child: Numbutton(".", color: Colors.white),
                  ),
                ),
                Expanded(
                  child: Container(
                    height: buttonHeight,
                    child: Numbutton("/"),
                  ),
                ),
              ],
            ),
          ],
        ),
      ),
    );
  }
}
