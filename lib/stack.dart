class Stack<E> {
  final List<E> _stack;

  Stack() : _stack = <E>[];

  bool get isEmpty => _stack.isEmpty;

  bool get isNotEmpty => _stack.isNotEmpty;

  int get size => _stack.length;

  Iterable<E> get content => _stack.reversed;

  void push(E e) => _stack.add(e);

  E pop() {
    if (_stack.isEmpty) throw StackEmptyException;
    return _stack.removeLast();
  }

  E get top {
    if (_stack.isEmpty) throw StackEmptyException;
    return _stack.last;
  }
}



class StackEmptyException implements Exception {
  const StackEmptyException();

  String toString() => 'StackEmptyException';
}
