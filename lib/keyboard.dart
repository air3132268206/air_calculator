import 'package:air_calculator/utils/date_util.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'c_data.dart';
import 'calculator.dart';
import 'history_list.dart';

class Numbutton extends StatelessWidget {
  var _text = '';
  Color color = Colors.white;

  Numbutton(this._text, {this.color});

  void add() {
    List dataList = Calculator.getDataList(Cdata.data);
    List opList = Calculator.getOpList(Cdata.data);
    // 判断首字母是否是 *，+，/
    if (Cdata.data == "") {
      if (_text == "/" || _text == "*" || _text == "+") {
        return;
      }
    }

    if (Cdata.data != "") {
      String lastOp =
          Cdata.data.substring(Cdata.data.length - 1, Cdata.data.length);
      // 判断 “/" 后面是否是0
//      if (lastOp == "/") {
//        if (_text == "0") {
//          return;
//        }
//      }
      if (lastOp == "-") {
        if (_text == "-") {
          return;
        }
      }

      if(Cdata.baseOffset==Cdata.data.length){
        if(Cdata.baseVerify(Cdata.preData)){
          if (opList.length > 0) {
            if (opList.last == "*-" || opList.last == "+-" || opList.last == "/-") {
              if (Calculator.isOp(_text)) {
                Cdata.setData(Cdata.data.replaceFirst(
                    Cdata.data.substring(Cdata.data.length - 1, Cdata.data.length),
                    "",
                    Cdata.data.length - 1));
                Cdata.setData(Cdata.data.replaceFirst(
                    Cdata.data.substring(Cdata.data.length - 1, Cdata.data.length),
                    _text,
                    Cdata.data.length - 1));
                Cdata.baseOffset=Cdata.data.length;
                Cdata.setCursor();
              }
            }
          }

          // 校验 是否是连续两个操作符
          if (Calculator.isOp(lastOp) || Calculator.isPoint(lastOp)) {
            if (_text == "+" || _text == "*" || _text == "/" || _text == ".") {
              Cdata.setData(Cdata.data.replaceFirst(
                  Cdata.data.substring(Cdata.data.length - 1, Cdata.data.length),
                  _text,
                  Cdata.data.length - 1));
              Cdata.setCursor();
              return;
            }
          }
        }
      }


      // 检查首字母是否是 *，+，/
      if (dataList.length < 1) {
        if (_text == "*" || _text == "/" || _text == "+") {
          return;
        }
      }


    }

    if (dataList.length > 0) {
      // 检查操作数是否有两个“.”
      if (dataList.last.toString().indexOf(".") > 0 && _text == ".") {
        return;
      }
    }

    Cdata.addData(this._text);
  }

  @override
  Widget build(BuildContext context) {
    return new RaisedButton(
      key: Key(""),
      onPressed: add,
      focusColor: this.color,
      color: this.color,
      textColor: Colors.black,
      child: Text(
        this._text,
        style: TextStyle(fontSize: 28),
      ),
      shape: RoundedRectangleBorder(
          side: BorderSide(width: 0.05, color: Colors.grey)),
    );
  }
}

class CancelButton extends StatelessWidget {
  void cancel() {
    Cdata.cancelData();
  }

  @override
  Widget build(BuildContext context) {
    return new RaisedButton(
      key: Key(""),
      onPressed: cancel,
      textColor: Colors.black,
      child: Text(
        "C",
        style: TextStyle(fontSize: 28),
      ),
      shape: RoundedRectangleBorder(
          side: BorderSide(width: 0.05, color: Colors.grey)),
    );
  }
}

class DeleteButton extends StatelessWidget {
  void delete() {
    if (Cdata.data.length > 0) {
//      Cdata.setData(Cdata.data.replaceFirst(
//          Cdata.data.substring(Cdata.data.length - 1, Cdata.data.length),
//          "",
//          Cdata.data.length - 1));
      if(Cdata.baseOffset<=0){
        return;
      }

      Cdata.setData(Cdata.data.replaceFirst(
          Cdata.data.substring(Cdata.baseOffset - 1, Cdata.baseOffset),
          "",
          Cdata.baseOffset - 1));
      Cdata.baseOffset--;
      Cdata.setCursor();
    }
  }

  @override
  Widget build(BuildContext context) {
    return new RaisedButton(
      key: Key(""),
      onPressed: delete,
      textColor: Colors.black,
      child: Icon(Icons.backspace),
      shape: RoundedRectangleBorder(
          side: BorderSide(width: 0.05, color: Colors.grey)),
    );
  }
}

class RotateButton extends StatelessWidget {
  void rotate() {
    List<String> dataList = Calculator.getDataList(Cdata.data);
    if (dataList.length == 0) {
      return;
    }
    if (dataList.length == 1) {
      Cdata.data = (double.parse(dataList[0]) * -1).toString();
      Cdata.editingController.text = Cdata.data;
      return;
    }

    String str;
    List opList = Calculator.getOpList(Cdata.data);
    for (int i = 0; i < dataList.length; i++) {
      if (i == 0) {
        str = dataList[dataList.length - 1 - i];
        str = str + opList[i];
      } else {
        str = str + dataList[dataList.length - 1 - i];
        if (i < dataList.length - 1) {
          str = str + opList[i];
        }
      }
    }
    Cdata.setData(str);
  }

  @override
  Widget build(BuildContext context) {
    return new RaisedButton(
      key: Key(""),
      onPressed: rotate,
      textColor: Colors.black,
      color: Colors.white,
      child: Icon(Icons.crop_rotate),
      shape: RoundedRectangleBorder(
          side: BorderSide(width: 0.05, color: Colors.white)),
    );
  }
}

class CalculatorButton extends StatelessWidget {
  void calculator() {
    String temp = Cdata.data;
    if (temp == '' || temp == null) {
      return;
    }
    String lastOp =
        Cdata.data.substring(Cdata.data.length - 1, Cdata.data.length);
    // 如果最后一位是操作符，则在计算时删除最后一位
    if (lastOp == "+" || lastOp == "-" || lastOp == "*" || lastOp == "/") {
      Cdata.setData(Cdata.data.replaceFirst(
          Cdata.data.substring(Cdata.data.length - 1, Cdata.data.length),
          "",
          Cdata.data.length - 1));
    }

    //
    List dataList = Calculator.getDataList(Cdata.data);
    List opList = Calculator.getOpList(Cdata.data);
    if (dataList.length <= 1) {
      return;
    }

    Cdata.value = Calculator.calculator(dataList, opList);
    Cdata.cal();

    // 计算结果放到缓存中
    HistoryList.setHistoryList(
        DateUtil.getTime() + ',' + temp + "=" + Cdata.value.toString());
  }

  @override
  Widget build(BuildContext context) {
    return new RaisedButton(
      key: Key(""),
      onPressed: calculator,
      textColor: Colors.black,
      child: Text(
        "=",
        style: TextStyle(fontSize: 28),
      ),
      shape: RoundedRectangleBorder(
          side: BorderSide(width: 0.05, color: Colors.grey)),
    );
  }
}
