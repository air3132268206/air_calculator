import 'dart:core';
import 'package:air_calculator/stack.dart' as cStack;
import 'package:common_utils/common_utils.dart';
import 'c_data.dart';

class Calculator {
  static double calculator(List dataList, List opList) {
    if (dataList.isEmpty) {
      return double.parse(dataList[0]);
    }

    // 数据栈
    cStack.Stack<double> s = cStack.Stack<double>();
    // 操作符栈
    cStack.Stack<String> op = cStack.Stack<String>();

    int j = 0;
    for (int i = 0; i < dataList.length; i++) {
      s.push(double.parse(dataList[i]));
      if (op.isEmpty && j < dataList.length - 1) {
        op.push(opList[j++]);
      } else {
        if (j < dataList.length - 1) {
          // 操作符栈的栈顶元素优先级高
          if (checkPriority(opList[j], op.top)) {
            while (op.isNotEmpty) {
              double num2 = s.pop();
              double num1 = s.pop();
              double res = culOp(num1, num2, op.pop());
              s.push(res);
              if (op.isNotEmpty && !checkPriority(opList[j], op.top)) {
                break;
              }
            }
          }
          op.push(opList[j++]);
        } else {
          if (op.top == "*" || op.top == '/') {
            double num2 = s.pop();
            double num1 = s.pop();
            double res = culOp(num1, num2, op.pop());
            s.push(res);
          }
        }
      }
    }

    // 最后操作加减法
    while (op.isNotEmpty) {
      double num2 = s.pop();
      double num1 = s.pop();
      double res = culOp(num1, num2, op.pop());
      s.push(res);
    }

    return s.top;
  }

  static double culOp(double num1, double num2, String op) {
    switch (op) {
      case "*":
        {
          return NumUtil.multiply(num1, num2);
        }
      case "/":
        {
          return NumUtil.divide(num1, num2);
        }
      case "+":
        {
          return NumUtil.add(num1, num2);
        }
      case "-":
        {
          return NumUtil.subtract(num1, num2);
        }
      case "*-":
        {
          return NumUtil.multiply(num1, -num2);
        }
      case "+-":
        {
          return NumUtil.subtract(num1, num2);
        }
      case "/-":
        {
          return NumUtil.divide(num1, -num2);
        }
      case "--":
        {
          return NumUtil.add(num1, num2);
        }
    }
    return 0.0;
  }

  ///
  /// 校验优先级
  /// op : 当前操作符
  /// topOp ： 栈顶操作符
  ///
  static bool checkPriority(String op, String topOp) {
    if (topOp == '*' || topOp == '/') {
      return true;
    }
    if (op == '*' || op == '/') {
      return false;
    }

    if ((topOp == '-' || topOp == '+') && (op == "+" || op == "-")) {
      return true;
    }
    return false;
  }

  static List getDataList(String string) {
    List<String> dataList =
        Cdata.data.split(new RegExp(r"(?<=[^!e\+])\+|\-|\*|\/"));
    dataList.removeWhere((item) => item == '' || item == null);
    if (dataList.length > 0) {
      // 对于首字符为 “-” 的特殊处理
      if (Cdata.data.substring(0, 1) == "-") {
        dataList[0] = "-" + dataList[0];
      }
    }
    return dataList;
  }

  static List getOpList(String string) {
    List opList = Cdata.data.split(new RegExp(r"[0-9|\.]|e\+"));
    opList.removeWhere((item) => item == '' || item == null);
    if (opList.length > 0) {
      // 对于首字符为 “-” 的特殊处理
      if (Cdata.data.substring(0, 1) == "-") {
        opList.removeAt(0);
      }
    }
    return opList;
  }

  static bool isOp(String string) {
    if (string == "+" || string == "*" || string == "-" || string == "/") {
      return true;
    } else {
      return false;
    }
  }

  static bool isPoint(String string) {
    if (string == '.') {
      return true;
    }
    return false;
  }
}
