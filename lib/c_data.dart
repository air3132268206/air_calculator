
import 'package:flutter/cupertino.dart';

class Cdata {
  static int baseOffset=0;


  static ValueNotifier<List> historyListV = ValueNotifier<List>(
      Cdata.historyList
  );
  static ScrollController listViewController = ScrollController();
  static TextEditingController editingController = new TextEditingController();
  static var data = '';
  static String preData=data;
  static double value = 0;
  static List historyList=new List();

  static void setData(String string) {
    data = string;
    editingController.text = data;
  }

  static String addData(String string) {
//    data = data + string;
//    editingController.text = data;
    if(baseOffset==data.length){
      String temp=data+string;
      preData=temp;
      if(!baseVerify(temp)){
        editingController.text = data;
      }else{
        data = temp;
        editingController.text = data;
        baseOffset++;
      }

    }else{
      String tempEnd=data.substring(baseOffset);
      String tempStart=data.substring(0,baseOffset);
      tempStart=tempStart+string;
      preData=tempStart+tempEnd;
      if(!baseVerify(preData)){
        editingController.text=data;
      }else{
        data=preData;
        editingController.text=data;
        baseOffset++;
      }
    }
    setCursor();
    return data;
  }

  static String cancelData() {
    data = '';
    editingController.text = data;
    _init();
  }

  static String cal() {
    data = value.toString();
    editingController.text = data;
    baseOffset=data.length;
    setCursor();

  }

  /// 设置光标位置
  static void setCursor(){
    editingController.selection=TextSelection.collapsed(offset: baseOffset);
    editingController.selection=TextSelection(baseOffset: baseOffset,extentOffset: baseOffset);
  }

  /// 基础校验,校验表达式是否符合规则
  static bool baseVerify(String string){
    RegExp mobile = new RegExp(r"\*\*|\+\+|---|\/\/|\+\*|\+\/|\+--|\*\+|\*\/|\*--|\/\+|\/\*|\/--|-\+|-\*|-\/|e\+-"
    r"|\+-\*|\+-\/|\+-\+|\*-\*|\*-\/|\*-\+|\/-\/|\/-\+|\/-\*|^--");
    return !mobile.hasMatch(string);
  }

  static void _init(){
    baseOffset=0;
    setCursor();
  }



}
