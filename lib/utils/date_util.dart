import 'package:intl/intl.dart';

class DateUtil{
  static String getTime(){
    var now = new DateTime.now();
    var formatter = new DateFormat('yyyy-MM-dd H:m:s');
    String formatted = formatter.format(now);
    return formatted;
  }

  static String getDate(){
    var now = new DateTime.now();
    var formatter = new DateFormat('yyyy-MM-dd');
    String formatted = formatter.format(now);
    return formatted;
  }
}