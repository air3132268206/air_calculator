import 'package:shared_preferences/shared_preferences.dart';
import 'c_data.dart';

class HistoryList {
  // 获取缓存
  static Future<List<String>> getHistoryList() async {
    SharedPreferences sharedPreferences = await SharedPreferences.getInstance();
    List list = sharedPreferences.getStringList("history-list");
    Cdata.historyList = list;
    Cdata.historyListV.value = list;
    return sharedPreferences.getStringList("history-list");
  }

  /// 设置缓存
  static Future<void> setHistoryList(String string) async {
    SharedPreferences sharedPreferences = await SharedPreferences.getInstance();
    List<String> list = sharedPreferences.getStringList("history-list");
    if (list == null) {
      list = new List();
    }
    if (list.length >= 100) {
      list.removeAt(0);
    }
    if (string == null || string == "") {
    } else {
      list.add(string);
    }
    Cdata.historyList = list;
    Cdata.historyListV.value = list;
    sharedPreferences.setStringList("history-list", list);
    Cdata.listViewController
        .jumpTo(Cdata.listViewController.position.maxScrollExtent + 80.0);
  }

  /// 同步缓存
  static Future<void> syncHistoryList() async {
    SharedPreferences sharedPreferences = await SharedPreferences.getInstance();
    sharedPreferences.setStringList("history-list", Cdata.historyList);
    Cdata.listViewController
        .jumpTo(Cdata.listViewController.position.maxScrollExtent + 80.0);
  }

  // 删除缓存
  static Future<void> deleteHistoryList() async {
    List<String> list = new List();
    Cdata.historyList = list;
    Cdata.historyListV.value = list;

    SharedPreferences sharedPreferences = await SharedPreferences.getInstance();
    sharedPreferences.setStringList("history-list", Cdata.historyList);
  }
}
