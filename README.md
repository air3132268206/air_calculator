# air_calculator（菲菲计算器）

## 开发源动力：华为手机自带计算器一直没有历史记录功能，不好用，而其他计算器又有广告，女朋友很不喜欢（后果很严重），所以有了air_calculator（菲菲计算器）

## 软件特色：
1. 轻量级，无广告
2. 满足日常需求
3. 采用flutter开发


## 界面展示
![输入图片说明](https://images.gitee.com/uploads/images/2020/0430/142702_ad3ad88a_1737689.gif "深度录屏__20200430142349.gif")

![输入图片说明](https://images.gitee.com/uploads/images/2020/0430/142238_262c7ef3_1737689.png "Screenshot_1588227593.png")

![输入图片说明](https://images.gitee.com/uploads/images/2020/0430/142315_712a80ac_1737689.png "Screenshot_1588227786.png")


# 个人官网
[https://www.airfei.cn/](https://www.airfei.cn/)





